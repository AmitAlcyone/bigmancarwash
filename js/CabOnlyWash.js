﻿var CabOnlyWash = CabOnlyWash || {};

$(document).ready(function () {
    CabOnlyWash.initialize();

});
var CabOnlyWash = {
    LocationId: null,
    LaneNumber: 0,
    PortalLinkId: 0,
    TruckType: null,
    TermCondition: null,
    selectedbookTypeId: 0,
    pb: null,
    CustomerCode: null,
    CustomerUrl: null,
    PortalLinkToken: null,
    BaseURL: null,
    BaseURLSignalR: null,
    BigManServicesListArray: [],
    initialize: function () {
        CabOnlyWash.cryptStrigUrl = CabOnlyWash.getUrlParameter("TransactionCode");
        CabOnlyWash.BaseURL = '';

        CabOnlyWash.CustomerUrl = null
        CabOnlyWash.PortalLinkToken = null;
        CabOnlyWash.LocationId = null
        CabOnlyWash.LaneNumber = null
        CabOnlyWash.PortalLinkId = null
        CabOnlyWash.TruckType = null
        //CabOnlyWash.BaseURLSignalR = "https://devapi.washassist.com/api";
        CabOnlyWash.BaseURLSignalR = "https://stagingapi.washassist.com/api";
        CabOnlyWash.FormValidation();
        CabOnlyWash.attachEvents();
        CabOnlyWash.LoadWaitDialog();
        CabOnlyWash.GetDecryptUrlValue(CabOnlyWash.cryptStrigUrl);
    },
    GetDecryptUrlValue: function (Param) {

        try {
            var cryptUrl = {
                name: Param
            }
            $.ajax({
                type: "POST",
                url: CabOnlyWash.BaseURL + '/BMTWPortalLink/DecryptUrlPerameter',
                data: JSON.stringify(cryptUrl),
                contentType: "application/json",
                success: function (data, status, xhr) {
                    if (data.statusCode == 200) {
                        console.log(data.result);
                       // CabOnlyWash.CustomerUrl = CabOnlyWash.getUrlParameter("customerurl");

                        CabOnlyWash.CustomerUrl = CabOnlyWash.getParameterfromStringBaseUrl('customerurl', data.result)
                        CabOnlyWash.PortalLinkToken = CabOnlyWash.getParameterfromString('portallinktoken', data.result);
                        CabOnlyWash.LocationId = CabOnlyWash.getParameterfromString('locationId', data.result)
                        CabOnlyWash.LaneNumber = CabOnlyWash.getParameterfromString('laneNumber', data.result)
                        CabOnlyWash.TruckType = CabOnlyWash.getParameterfromString('truckType', data.result)
                        CabOnlyWash.PortalLinkId = CabOnlyWash.getParameterfromString('portalLinkId', data.result)

                        if (CabOnlyWash.CheckUrlIsValid()) {
                            CabOnlyWash.GetServices();
                            CabOnlyWash.GetCreditcardKeys();
                            CabOnlyWash.GetTemplateData();
                        }
                    }
                    else {
                        swal("Error", data.responseMessage, "error");
                    }
                },
                error: function (xhr, status, error) {
                    swal("Error", "Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText, "error");
                }
            });
        } catch (_err) {
            window.prompt("Copy to clipboard: Ctrl+C, Enter", _err.stack);
        }

    },

    attachEvents: function () {
        if (!(/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))) {
            $('#txtCellPhone').formatter({
                pattern: '({{999}}) {{999}} {{9999}}'
            });
        }
        //$("form[name='formCheckOut'] input").off('keypress , onchange');
        //$("form[name='formCheckOut'] input").on('keypress , onchange', function () {
        //    if ((/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))) {            
        //        $("form[name='formCheckOut']").valid();
        //    }
        //    return;
        //});

        //$('#terms').unbind('click');
        //$('#terms').click(function () {
        //    if ($('#terms').prop('checked') != true) {
        //        $('#terms').removeClass('accent');
        //        $('#term_lbl').addClass('hide_term_lbl');
        //        $('#terms').addClass('check_Danger');
        //    }
        //    else {
        //        $('#term_lbl').removeClass('hide_term_lbl');
        //        $('#terms').addClass('accent');
        //        $('#terms').removeClass('check_Danger');
        //    }
        //});
        $('#logoImage').unbind('click');
        $('#logoImage').click(function () {
            window.location.reload();
        });

        $('#toFirst').unbind('click');
        $('#toFirst').click(function () {
            $('#txtCellPhone').val('');
            $(".payment_sec").hide();
            $(".services_sec").fadeIn();
        });

        $('.selectBtn').unbind('click')
        $('.selectBtn').click(function (e) {
            //var validator = $("#formCheckOut").validate();
            //validator.resetForm();
            $('#MobilePhoneValidation').text('');
            $('#MobilePhoneValidation').addClass('hide');
            var url = CabOnlyWash.PCIScript;
            $.getScript(url, function () {
                CabOnlyWash.InitializePCI();
            });
            CabOnlyWash.selectedbookTypeId = 0;
            CabOnlyWash.selectedbookTypeId = $(this).attr("data-lBookTypeId");
            var NewplanRowData = CabOnlyWash.BigManServicesListArray.find(element => element.lBookTypeId == CabOnlyWash.selectedbookTypeId);
            var TaxAmount = 0;
            if (NewplanRowData.bTaxable) {
                if (NewplanRowData.dblTaxRate != null && NewplanRowData.dblTaxRate != undefined) {
                    TaxAmount = NewplanRowData.dblBookPrice * (NewplanRowData.dblTaxRate / 100)
                }
            }
            $('#dblBookPrice').html('$' + (NewplanRowData.dblBookPrice).toFixed(2));
            $('#taxAmount').html('$' + TaxAmount.toFixed(2));
            $('#totalAmount').html('$' + (NewplanRowData.dblBookPrice + TaxAmount).toFixed(2));
            $(".services_sec").hide();
            $(".payment_sec").fadeIn();

        });

    },

    CheckUrlIsValid: function () {
        if (CabOnlyWash.LaneNumber != null && Number(CabOnlyWash.LaneNumber) > 0 && CabOnlyWash.LocationId != null && CabOnlyWash.LocationId != "" && CabOnlyWash.TruckType != null && CabOnlyWash.TruckType != "") {
            return true;
        }
        else {
            swal('Error', 'Invalid Url:Please Check LaneNumber,LocationId and TruckType.', 'error');
            return false
        }


    },

    IsNumberKey: function (event) {
        if (!(/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))) {
            $('#txtCellPhone').on('input', function (event) {
                this.value = this.value.replace(/[^0-9]/g, '');
                $('#txtCellPhone').formatter({
                    pattern: '({{999}}) {{999}} {{9999}}'
                });
                $('#txtCellPhone').attr('maxlength', 14);
            });
            if ($('#txtCellPhone').val().length != 14) {
                $('#MobilePhoneValidation').removeClass('hide');
                $('#credit-card-submit').addClass('creditcheckouthide');
                $('#MobilePhoneValidation').text('Your Phone Number must be 10 digit..');
            }
            else {
                $('#MobilePhoneValidation').text('');
                $('#MobilePhoneValidation').addClass('hide');
                $('#credit-card-submit').removeClass('creditcheckouthide');
            }
        } else {
            $('#txtCellPhone').on('input', function (event) {
                this.value = this.value.replace(/[^0-9]/g, '');
            });
            $('#txtCellPhone').attr('maxlength', 10);
            if ($('#txtCellPhone').val().length != 10) {
                $('#MobilePhoneValidation').removeClass('hide');
                $('#credit-card-submit').addClass('creditcheckouthide');
                $('#MobilePhoneValidation').text('Your Phone Number must be 10 digit.');
            }
            else {
                $('#MobilePhoneValidation').text('');
                $('#MobilePhoneValidation').addClass('hide');
                $('#credit-card-submit').removeClass('creditcheckouthide');
            }
        }

    },

    GetServices: function () {
        try {
            if (CabOnlyWash.TruckType == "CabAndTrailer") {
                CabOnlyWash.TruckType = "Truck"
            }
            var postData = {
                customerUrl: CabOnlyWash.CustomerUrl,
                PortlLinkToken: CabOnlyWash.PortalLinkToken,
                TruckType: CabOnlyWash.TruckType
            }
            $.ajax({
                type: "POST",
                url: CabOnlyWash.BaseURL + '/BMTWPortalLink/GetBMTWPortalLinkService',
                data: postData,
                success: function (result, status, xhr) {
                    if (result.statusCode == 200) {
                        var res = JSON.parse(result.result);
                        CabOnlyWash.BigManServicesListArray = [];
                        $.each(res, function (index, item) {
                            var services = "";
                            if (res != null && res != undefined) {
                                services = '<div class="card_box"><div class="qr_code_img" ><img src="https://devcp.washassist.com/images/wash_icon_home_2.png" alt=""></div><div class="card_info"><div class="wash_name">' + item.sBookDescription + '</div><div class="wash_price">$' +
                                    item.dblBookPrice + '</div><button type="button" data-lBookTypeId="' + item.lBookTypeId + '" class="bw_btn gra_btn selectBtn">Select</button></div></div>'

                                $(".service_list").append(services);
                            }

                        });
                        CabOnlyWash.BigManServicesListArray = res;
                        CabOnlyWash.attachEvents();
                    }
                    else {
                        swal("Error", result.responseMessage, "error");
                    }
                },
                error: function (xhr, status, error) {
                    swal("Error", "Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText, "error");
                }
            });
        } catch (_err) {
            window.prompt("Copy to clipboard: Ctrl+C, Enter", _err.stack);
        }
    },
    GetTemplateData: function () {
        try {
            var postData = {
                customerUrl: CabOnlyWash.CustomerUrl,
                PortalLinkToken: CabOnlyWash.PortalLinkToken
            }
            $.ajax({
                type: "POST",
                url: CabOnlyWash.BaseURL + '/BMTWPortalLink/GetBigWashTemplateData',
                data: postData,
                success: function (result, status, xhr) {
                    if (result.statusCode == 200) {
                        var res = result.result;
                        console.log(res)
                        /* CabOnlyWash.LocationId = res.LocationId;*/
                        /* var LaneNumber = (res.LaneNumber).split('_');*/
                        /* CabOnlyWash.LaneNumber = LaneNumber[1];*/
                        CabOnlyWash.TermCondition = res.TemplateTermCondition;
                        CabOnlyWash.CustomerCode = res.CustomerCode;
                        if (res.TemplateTermCondition != null && res.TemplateTermCondition != '') {
                            $('#TermsandConditions').html(res.TemplateTermCondition)
                        }
                        else {
                            CabOnlyWash.TermCondition = '<p style="text-align: center;">No Terms and Conditions </p>';
                            $('#TermsandConditions').html(CabOnlyWash.TermCondition)
                        }

                    }
                    else {
                        swal("Error", result.responseMessage, "error");
                    }
                },
                error: function (xhr, status, error) {
                    swal("Error", "Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText, "error");
                }
            });
        } catch (_err) {
            window.prompt("Copy to clipboard: Ctrl+C, Enter", _err.stack);
        }
    },
    blockUI: function (element, block) {

        if (block) {
            var imageUrl = "./images/loading-black.gif";
            var width = $(element).width();
            var height = $(element).height();
            $(element).block({
                message: "<img src='" + imageUrl + "'/>",
                css: {
                    padding: 0,
                    margin: 0,
                    width: '100%',
                    top: '100%',
                    left: '35%',
                    textAlign: 'center',
                    color: '#000',
                    border: '0px',
                    cursor: 'wait',
                    opacity: 0.3
                },
                overlayCSS: { backgroundColor: '#FFFFFF', opacity: 0.3 }
            });
        }
    },
    unblockUI: function (element) {
        $(element).unblock();
    },
    LoadWaitDialog: function () {
        $(document).ajaxStart(function () {
            CabOnlyWash.blockUI($(".section_loader"), true);
        }).ajaxStop(function () {
            CabOnlyWash.unblockUI(".section_loader")
        }).ajaxError(function () {
            CabOnlyWash.unblockUI(".section_loader")
        })
    },
    // Get keys, which are needs to show credit card UI.
    GetCreditcardKeys: function () {
        try {
            var postData = {
                customerUrl: CabOnlyWash.CustomerUrl,
            }
            $.ajax({
                type: "POST",
                url: CabOnlyWash.BaseURL + '/BMTWPortalLink/GetKey',
                data: postData,

                success: function (result, status, xhr) {
                    if (result.statusCode == 200) {
                        var data = JSON.parse(result.result);
                        CabOnlyWash.IsPaymentLive = true;
                        CabOnlyWash.Enviornment = data.Enviornment;
                        CabOnlyWash.PCIScript = data.PCIScript;
                        CabOnlyWash.XGPApiKey = data.XGPApiKey;

                    }
                    else {
                        swal("Error", result.responseMessage, "error");
                    }
                },
                error: function (xhr, status, error) {
                    swal("Error", "Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText, "error");
                }
            });
        } catch (_err) {
            window.prompt("Copy to clipboard: Ctrl+C, Enter", _err.stack);
        }
    },
    Checkout: function () {
        try {
            if ($('#terms').prop('checked') != true) {
                swal("Error", "Term & Condition required.", "error");
                //$('#terms').removeClass('accent');
                //$('#term_lbl').removeClass('hide_term_lbl');
                //$('#terms').addClass('check_Danger');
                return;
            }
            //else {
            //    $('#terms').addClass('accent');
            //    $('#term_lbl').removeClass('hide_term_lbl');
            //    $('#terms').removeClass('check_Danger');
            //}
            var phoneNumber = $('#txtCellPhone').val();
            phoneNumber = phoneNumber.replace(/([() ])+/g, '');
            var postData = {
                customerCode: CabOnlyWash.CustomerCode,
                PhoneNumber: phoneNumber,
                temporary_token: CabOnlyWash.temporary_token,
                bookTypeId: CabOnlyWash.selectedbookTypeId,
                IsSendSms: true,
                unixTime: Date.now(),
                locationId:CabOnlyWash.LocationId,
            };
            $.ajax({
                type: "POST",
                url: CabOnlyWash.BaseURL + "/BMTWPortalLink/BuyService",
                data: postData,
                success: function (result, status, xhr) {
                    if (result.statusCode == 200) {
                        var PB = JSON.parse(result.result);
                        CabOnlyWash.pb = PB;
                        console.log(PB);
                        //$("#divPortalLinkQRcode").html('');
                        //jQuery("#divPortalLinkQRcode").qrcode({
                        //    //render:"table"
                        //    width: 200,
                        //    height: 200,
                        //    text: PB.sBarcode,

                        //});
                        //CabOnlyWash.GetKeyForNotification();
                        $("#lblbarcode").text(PB.sBarcode);
                        if (CabOnlyWash.TruckType == "Truck") {
                            $(".payment_sec img").attr('src', "./images/Cab_trailer.png");
                        } else {

                            $(".payment_sec img").attr('src', "./images/cab_only.png");
                        }
                        $(".payment_sec").hide();
                        $(".thank_sec").fadeIn();
                        var url = CabOnlyWash.PCIScript;
                        $.getScript(url, function () {
                            CabOnlyWash.InitializePCI();
                        });
                    }
                    else {

                        swal("Error", result.responseMessage, "error");
                    }
                },
                error: function (xhr, status, error) {
                    swal("Error", "Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText, "error");
                }
            });
        } catch (_err) {
            window.prompt("Copy to clipboard: Ctrl+C, Enter", _err.stack);
        }
    },

    InitializePCI: function () {

        $("#credit-card-card-number").html("");
        $("#credit-card-card-expiration").html("");
        $("#credit-card-card-cvv").html("");
        $("#credit-card-submit").html("");

        var cardPlaceHolder = ".... .... .... ....";
        //var cardPlaceHolder = "���� ���� ���� ����";
        var expiryPlaceHolder = "MM / YYYY";
        var submitBtn = "CHECKOUT";
        var cardTraget = "#credit-card-card-number";
        var expiryTraget = "#credit-card-card-expiration";
        var cvvTraget = "#credit-card-card-cvv";
        var submitTraget = "#credit-card-submit";

        var cardForm = null;
        var xGP_API_Key = CabOnlyWash.XGPApiKey;
        var xGP_enviornment = CabOnlyWash.Enviornment;
        GlobalPayments.configure({
            'X-GP-Api-Key': xGP_API_Key,
            'X-GP-Environment': xGP_enviornment
        });
        cardForm = GlobalPayments.ui.form({
            fields: {
                "card-number": {
                    placeholder: cardPlaceHolder,
                    target: cardTraget
                },
                "card-expiration": {
                    placeholder: expiryPlaceHolder,
                    target: expiryTraget
                },
                "card-cvv": {
                    placeholder: "...",
                    //placeholder: "���",
                    target: cvvTraget,
                    type: "password"
                },
                "submit": {
                    text: submitBtn,
                    target: submitTraget
                },
            },
            styles: JSON.parse(CabOnlyWash.initializeGlobalCss()),
        });
        cardForm.on("token-success", (resp) => {
            CabOnlyWash.temporary_token = resp.temporary_token;
            // if ($("form[name='formCheckOut']").valid()) {
            $('#credit-card-submit').removeClass('creditcheckouthide');
            CabOnlyWash.Checkout();
            // }
        });

        cardForm.on("token-error", (resp) => {


            let error = resp.error;
            let validationDataset = [{
                isValid: (error.code === 'invalid_card') ? false : true,
                searchTerm: 'card_number', elementID: 'card-number',
                message: 'Invalid card number'
            },
            {
                isValid: true, searchTerm: 'expiry',
                elementID: 'card-expiration',
                message: 'Invalid expiration date'
            },
            {
                isValid: true, searchTerm: 'card_security_code',
                elementID: 'card-cvv', message: 'Invalid CVV'
            }];

            for (var validationData of validationDataset) {
                if (error.detail) {
                    for (var detail of error.detail) {
                        var data_path = detail.data_path || '';
                        var description = detail.description || '';
                        if (data_path.search(validationData.searchTerm) > -1 || description.search(validationData.searchTerm) > -1) { validationData.isValid = false; }
                    }
                } if (!validationData.isValid) {
                    showError(validationData.elementID, validationData.message);
                }
            }

            function showError(elementID, msg) {
                swal("Credit Card Validation", "Please enter valid Credit Card information.", "error");
            }
        });
    },
    initializeGlobalCss: function () {
        var globalPayCss = JSON.stringify(
            {
                "button": {
                    "background": "#0065B0",
                    "background-image": "linear-gradient(to right,rgb(87, 164, 255),rgb(0, 79, 172),rgb(87, 164, 255),rgb(0, 79, 172))",
                    "background-size": "300% 100%",
                    "width": "100% !important",
                    "font-size": "13px",
                    "textTransformation": "uppercase",
                    "border": "1px solid rgb(87, 164, 255)",
                    "border-radius": "5px",
                    "cursor": "pointer",
                    "outline": "none",
                    "color": "#fff",
                    "height": "40px",
                    "font-weight": "600",
                    "letter-spacing": "1px",
                    "line-height": "1.5",
                    "max-width": "120px",
                    "position": "absolute",
                    "right": "0",
                    "transition": "all .4s ease-in-out"
                },
                "button:hover": {
                    "background-position": "100% 0",
                },
                "input": {
                    "display": "block",
                    "background": "none",
                    "border": "none",
                    "border-bottom": "1px solid #fefefe",
                    "font-size": "1.275rem",
                    "color": "black",
                    "width": "100%",
                    "text-align": "center",
                },
                "input.card-number": {
                    "letter-spacing": "0.15rem",
                    "border": "1px solid #dedede",
                    "border-radius": "3px",
                    "margin-bottom": "0px",
                    "width": "100% !important",
                    "height": "34px",
                    "box-sizing": "border-box",
                    "font-size": "15px",
                    "outline": "none",
                    "color": "#363636",
                    "background-color": "#ffffff!important",
                    "padding": "0.375rem 0.75rem;",
                },
                "input.card-expiration": {
                    "border": "1px solid #dedede",
                    "border-radius": "3px",
                    "margin-bottom": "10px",
                    "width": "100% !important",
                    "height": "34px",
                    "box-sizing": "border-box",
                    "font-size": "15px",
                    "outline": "none",
                    "color": "#363636",
                    "background-color": "#ffffff!important",
                    "padding": "0.375rem 0.75rem;",
                },
                "input.card-cvv": {
                    "border": "1px solid #dedede",
                    "border-radius": "3px",
                    "margin-bottom": "10px",
                    "width": "100% !important",
                    "height": "34px",
                    "box-sizing": "border-box",
                    "font-size": "15px",
                    "outline": "none",
                    "color": "#363636",
                    "background-color": "#ffffff!important",
                    "padding": "0.375rem 0.75rem;",

                },
                "input.invalid": {
                    "border-color": "#dc3545 !important;"
                },
                "input:focus": {
                    "-webkit-box-shadow": "inset 0px 0px 3px 0px #b0dcff !important", "box-shadow": "inset 0px 0px 3px 0px #b0dcff !important", "border-color": "#b0dcff !important"
                },
                "input": { "border-width": "1px" },
            });

        return globalPayCss;
    },
    FormValidation: function () {

        $.validator.addMethod('USformate', function (value) {
            if (/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(value)) {
                return true;
            }
            else {
                return false;
            }
        }, 'Phone number formate is invalid');

        $.validator.addMethod('RestricSpaicalChar', function (value, element) {
            if (/^[a-zA-Z0-9_ ]*$/.test(value)) {
                return true;
            }
            else {
                $(element).val(value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, ''));
                return false
            }
        }, 'Only allow alphabat or number ');

        $.validator.addMethod('Allowonlynumber', function (value, element) {
            if (/^[0-9]*$/.test(value)) {
                return true;
            }
            else {
                $(element).val(value.replace(/\D/g, ''));
                return false
            }
        }, "Only number is allow.");


        //$("form[name='formCheckOut']").validate({
        //    rules: {
        //        txtCellPhone: {
        //            required: true,
        //            minlength: 14,
        //            maxlength: 14,
        //            USformate: true
        //        },
        //    },
        //    messages: {
        //        txtCellPhone: {
        //            required: "Please enter Phone Number",
        //            minlength: "Your Phone Number must be at least 10 digit."
        //        },
        //    },
        //    invalidHandler: function (event, validator) {
        //        // 'this' refers to the form
        //        var errors = validator.numberOfInvalids();




        //        if (errors) {
        //            $('#txtCellPhone-error').show();
        //            $('#txtCellPhone-error').text(errors);
        //        } 
        //    }

        //});
    },
    getUrlParameter: function (sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            // sParameterName[1] = sURLVariables[i].split('=').slice(1).join('=');

            if (sParameterName[0] === sParam) {
                if (sParameterName[0] == 'baseurl') {
                    var newBase = sParameterName[1].split('//');
                    if (newBase[0].length == 5) {
                        newBase[0] = 'https:';
                    }
                    sParameterName[1] = newBase[0] + '//' + newBase[1];
                }
                return typeof sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
        return false;
    },
    getParameterfromString: function (sParam, queryParam) {

        var sURLVariables = queryParam.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {

                return typeof sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
        return false;
    },
    getParameterfromStringBaseUrl: function (sParam, queryParam) {

        var sURLVariables = queryParam.split('?'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {

                return typeof sParameterName[1] === undefined ? true : sParameterName[1].split('&')[0];
            }
        }
        return false;
    },
}